import numpy as np
import pandas as pd
from tqdm import tqdm
import sys
import bilby
import argparse
import gwpopulation
from gwpopulation_pipe.vt_helper import load_injection_data
import h5py
from scipy.interpolate import interp1d
import fitting_formulae as ff
from pop_models_nsbh import *


def load_eos_data(chi_max, nicer):
    # load EOS data
    if nicer:
        print('Using NICER file')
        eos_file = "/home/michael.zevin/projects/o4/gw230529/analysis_results/extreme_matter/inputs/LCEHL_EOS_posterior_samples_PSR+GW+NICER.h5"
    else:
        eos_file = "/home/michael.zevin/projects/o4/gw230529/analysis_results/extreme_matter/inputs/LCEHL_EOS_posterior_samples_PSR+GW.h5"

    eos_data = h5py.File(eos_file, "r")
    num_eos = len(eos_data["ns"].keys())
    eos_samples = []
    for i in range(num_eos):
        masses = eos_data["ns"]["eos_{}".format(i)]["M"]
        radii = eos_data["ns"]["eos_{}".format(i)]["R"]
        TOV_ind = np.argmax(masses)
        m_TOV = masses[TOV_ind]
        r_TOV = radii[TOV_ind]
        c_TOV = ff.compactness(m_TOV, r_TOV)
        chi_kep = ff.breakup_spin(c_TOV)
        chi_max = min(chi_max, chi_kep)
        mmax = ff.mmax_spinning(chi_max, chi_kep, m_TOV)
        mr_interp = interp1d(masses, radii, bounds_error=True)
        eos_samples.append(
            {
                "m_tov": m_TOV,
                "maximum_mass_pop": mmax,
                "tov_compactness": c_TOV,
                "chi_kep": chi_kep,
                "mr_interp": mr_interp,
            }
        )
    return eos_samples


def main(result_file, nicer):
    # load sensitivity injections
    injection_file = "/home/reed.essick/rates+pop/o1+o2+o3-sensitivity-estimates/LIGO-T2100377-v2/o1+o2+o3_nsbhpop_real+semianalytic-LIGO-T2100377-v2.hdf5"
    all_found_injections = load_injection_data(injection_file, ifar_threshold=4)
    all_found_injections = pd.DataFrame.from_dict(all_found_injections)
    eos_samples = load_eos_data(0.4, nicer)
    max_mass = [sample["maximum_mass_pop"] for sample in eos_samples]

    # choose appropriate mass ratio model
    if "gaussian" in result_file:
        q_model = truncnorm_q_nsbh
    elif "power_law" in result_file:
        q_model = power_law_q_nsbh
    else:
        raise ValueError(
            "Unrecognized pairing function, must be either gaussian or power_law!"
        )
    model = bilby.hyper.model.Model(
        [
            power_law_primary_wrapper,
            q_model,
            gwpopulation.models.redshift.PowerLawRedshift(z_max=0.25),
            beta_bh_spin,
        ]
    )
    result = bilby.core.result.read_in_result(result_file)

    remnant_mass = []

    for i, row in tqdm(result.posterior.iterrows(), total=len(result.posterior)):
        model._cached_parameters = {function: None for function in model.models}
        model.parameters = row.to_dict()
        weights = model.prob(all_found_injections) / all_found_injections["prior"]
        n_eff = np.sum(weights) ** 2 / np.sum(weights**2)
        samples = all_found_injections.sample(int(n_eff), weights=weights)
        inds = np.where(np.array(max_mass) >= row["m2_max"])[0]
        ind = np.random.choice(inds)
        eos_sample = eos_samples[ind]
        mrem = []
        for j, sample in samples.iterrows():
            m2 = sample["mass_1"] * sample["mass_ratio"]
            m0 = ff.m_nonspinning(sample["a_2"], float(eos_sample["chi_kep"]), m2)
            a1z = sample["a_1"] * sample["cos_tilt_1"]
            try:
                r0 = eos_sample["mr_interp"](m0)
                compactness = ff.compactness(m0, r0)
                mrem.append(
                    ff.mrem_msun_spinning(
                        sample["mass_ratio"], m2, compactness, a1z, sample["a_2"]
                    )
                )
            except ValueError:
                pass
        remnant_mass.append(np.array(mrem))

    frac_0 = [len(np.where(np.array(i) > 0)[0]) / len(i) for i in remnant_mass]
    frac_1 = [len(np.where(np.array(i) > 1e-1)[0]) / len(i) for i in remnant_mass]
    frac_2 = [len(np.where(np.array(i) > 1e-2)[0]) / len(i) for i in remnant_mass]
    if nicer:
        filename = result.outdir + "/" + result.label + "_embright_fraction_NICER_EOS.dat"
    else:
        filename = result.outdir + "/" + result.label + "_embright_fraction_realistic_EOS.dat"
    np.savetxt(
        filename,
        np.array([frac_0, frac_1, frac_2]).T,
        header="frac_0 \tfrac_1 \tfrac_2",
        delimiter="\t",
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--result-file', help='Bilby result file with hyper-posteriors')
    parser.add_argument('--nicer', action='store_true', help='include NICER EoS constraints')
    args = parser.parse_args()
    print(args)
    main(**vars(args))
