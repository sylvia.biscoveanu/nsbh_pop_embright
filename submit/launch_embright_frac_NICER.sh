#!/bin/bash
conda deactivate
conda activate gwpop_pipe
python calc_embright_fraction_realistic_EOS.py --result-file /home/michael.zevin/projects/o4/gw230529/analysis_results/populations/EMBright/final/gw_only_S230529ay_nsbh_ns_spin_pairing_gaussian_result.json --nicer
