#!/bin/bash
conda deactivate
conda activate gwpop_pipe
python nsbh_inference_S230529ay.py --S230529ay --outdir /home/michael.zevin/projects/o4/gw230529/analysis_results/populations/EMBright/final 2>&1 | tee outdir/s230529ay.out
