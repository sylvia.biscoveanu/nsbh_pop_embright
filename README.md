## NSBH population modeling
Code to infer the neutron star-black hole merger mass and spin distributions using [gwpopulation](https://github.com/ColmTalbot/gwpopulation/) and then to calculate the multimessenger prospects for such systems using the fitting formulae from [Foucart+ 2018](https://arxiv.org/abs/1807.00011) using the neutron star equation of state inference from [Legred+ 2022](https://zenodo.org/records/6502467). This method was originally presented in [Biscoveanu+ 2022](https://arxiv.org/abs/2207.01568).

- `fitting_formulae.py`: contains the remnant mass and eqaution of state fitting formulae required for the EM-bright calculation
- `pop_models_nsbh.py`: contains the NSBH mass and spin distributions
- `calc_embright_fraction_realistic_EOS.py`: performs the calculation of the EM-bright fraction given a population inference result file
- `nsbh_inference_S230529ay.py`: performs hierarchical inference to infer the hyper-parameters governing the NSBH mass and spin distributions

### Installation
In addition to cloning this repo, you will need to install bilby, gwpopulation, and gwpopulation_pipe in addition to standard packages like numpy, h5py, argparse, etc. All can be installed using pip.

### Running the Code
```
usage: nsbh_inference_S230529ay.py [-h] [--S230529ay] [--outdir OUTDIR]
                                   [--runtype {gaussian,power_law}]

optional arguments:
  -h, --help            show this help message and exit
  --S230529ay           include S230529ay
  --outdir OUTDIR       output directory
  --runtype {gaussian,power_law}
                        mass ratio model
```
Running `nsbh_inference_S230529ay.py` as indicated above will produce two files, one is a corner plot in png format showing the hyperparameter posteriors, and the other is a [bilby `result.json` file](https://lscsoft.docs.ligo.org/bilby/bilby-output.html) that contains the relevant meta data and posterior samples. In order to calculate the fraction of NSBH systems that may be EM bright for a given hierarchical inference result, you just need to run 
```
>>> python calc_embright_fraction_realistic_EOS.py my_result.json
```
This will produce a file called `{outdir}/{result_label}_embright_fraction_realistic_EOS.dat` where the `outdir` is what was specified in the call to `nsbh_inference_S230529ay.py` and the `result_label` depends on whether or not S230529ay was included in the analysis. This file contains the fraction of GW-detectable NSBH events that result in a remnant mass left outside the black hole ISCO radius > 0 Msun, > 0.001 Msun, and > 0.01 Msun, in that order, for each hyper-parameter posterior sample in the original result.json file.
