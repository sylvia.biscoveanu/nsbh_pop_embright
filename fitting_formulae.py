from gwpopulation.cupy_utils import xp
import numpy as np
import bilby

C_SI = bilby.core.utils.speed_of_light  # m/s
G_SI = bilby.core.utils.gravitational_constant  # m^3 kg^-1 s^-2
MSUN_SI = bilby.core.utils.solar_mass  # Kg

def compactness(mass, radius):
    """
    Neutron star compactness in SI units
    -----------
    Parameters:
    mass: NS mass in solar masses
    radius: NS radius in km
    """
    return G_SI * mass * MSUN_SI / (radius * 1000 * C_SI**2)

def r_isco(chi_BH):
    """
    Normalized ISCO radius from Eq. 3 of Foucart+ 2018
    arXiv:1807.00011
    -----------
    Parameters:
    chi_BH: algined component of BH spin
    """
    z1 = 1 + (1 - chi_BH ** 2) ** (1.0 / 3) * (
        (1 + chi_BH) ** (1.0 / 3) + (1 - chi_BH) ** (1.0 / 3)
    )
    z2 = xp.sqrt(3 * chi_BH ** 2 + z1 ** 2)
    return 3 + z2 - xp.sign(chi_BH) * xp.sqrt((3 - z1) * (3 + z1 + 2 * z2))


def mrem_norm(q, C_NS, chi_BH):
    """
    Normalized remnant mass in units of NS baryonic mass
    Eq. 4 of Foucart+ 2018, arXiv:1807.00011
    -----------
    Parameters:
    q: mass ratio
    C_NS: neutron star compactness
    chi_BH: aligned component of BH spin
    """
    eta = q / (1 + q) ** 2
    alpha = 0.406
    beta = 0.139
    gamma = 0.255
    delta = 1.761
    x = (
        alpha * (1 - 2 * C_NS) / eta ** (1.0 / 3)
        - beta * r_isco(chi_BH) * C_NS / eta
        + gamma
    )
    return xp.maximum(x, 0) ** delta

def mrem_msun(q, mass_NS, C_NS, chi_BH):
    baryonic_mass_NS = baryonic_mass_fit(mass_NS, C_NS)
    return mrem_norm(q, C_NS, chi_BH)*baryonic_mass_NS

def baryonic_mass_fit(mass_NS, C_NS):
    """
    Return baryonic mass in Msun from the fit in Eq. 8 
    of arXiv:1708.07714
    -----------
    Parameters:
    mass_NS: neutron star gravitational mass in Msun
    C_NS: neutron star compactness in SI units
    """
    a = 0.8858
    n = 1.2082
    return mass_NS*(1 + a * C_NS**n)

def spinning_baryonic_mass_fit(mass_NS, spin_NS):
    """
    Return baryonic mass in Msun from the fit in Eq. 20
    of arXiv:1506.05926
    -----------
    Parameters:
    mass_NS: neutron star gravitational mass in Msun
    spin_NS: neutron star dimensionless spin
    """
    return mass_NS + 13./200*mass_NS**2*(1 - 1./130*mass_NS**3.4*spin_NS**1.7)

def mrem_msun_spinning(q, mass_NS, C_NS, chi_BH, chi_NS):
    baryonic_mass_NS = spinning_baryonic_mass_fit(mass_NS, chi_NS)
    return mrem_norm(q, C_NS, chi_BH)*baryonic_mass_NS

def breakup_spin(c_TOV):
    """
    Return the breakup spin based on the quasi-universal relation
    for the critical mass, Eq. 5 of arXiv:2003.10391
    -----------
    Parameters:
    c_TOV: compactness of the neutron star at its TOV mass
    """
    alpha_1 = 0.045
    alpha_2 = 1.112
    return alpha_1/np.sqrt(c_TOV) + alpha_2*np.sqrt(c_TOV)

def mmax_spinning(ns_spin, chi_kep, m_TOV):
    """
    Return the maximum neutron star mass accounting for rigid 
    rotation, quasi-universal relation from Eq. 4 of arXiv:2003.10391
    -----------
    Parameters:
    ns_spin: dimensionless spin of the neutron star
    chi_kep: breakup spin computed above
    m_TOV: TOV mass for a particular EOS
    """
    a_1 = 0.132
    a_2 = 0.071
    return m_TOV*(1 + a_1 * (ns_spin/chi_kep)**2 + a_2 * (ns_spin/chi_kep)**4)

def m_nonspinning(ns_spin, chi_kep, m_spinning):
    """
    Inverse of mmax_spinning assuming the universal relation is valid
    for all masses, Eq. 12 of arXiv:2207.01568
    -----------
    Parameters:
    ns_spin: dimensionless spin of the neutron star
    chi_kep: breakup spin computed above
    m_spinning: gravitational mass of the spinning neutron star
    """
    a_1 = 0.132
    a_2 = 0.071
    return m_spinning/(1 + a_1 * (ns_spin/chi_kep)**2 + a_2 * (ns_spin/chi_kep)**4)
