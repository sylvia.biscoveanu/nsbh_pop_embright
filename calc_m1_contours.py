import sys
from pop_models_nsbh import power_law_primary_wrapper
from gwpopulation.cupy_utils import xp
import numpy as np
import bilby

filename_new = '/home/michael.zevin/projects/o4/gw230529/analysis_results/populations/EMBright/final/gw_only_S230529ay_nsbh_ns_spin_pairing_gaussian_result.json'
filename_old = '/home/michael.zevin/projects/o4/gw230529/analysis_results/populations/EMBright/final/gw_only_nsbh_ns_spin_pairing_gaussian_result.json'
result_new = bilby.core.result.read_in_result(filename_new)
result_old = bilby.core.result.read_in_result(filename_old)
downsampled_new = result_new.posterior.sample(1000)
downsampled_old = result_old.posterior.sample(1000)

m1s = np.linspace(2, 20, 500)
prob_m1_new = []
prob_m1_old = []

for i, row in downsampled_new.iterrows():
    pm1 = power_law_primary_wrapper({'mass_1': xp.asarray(m1s)}, row['alpha'], row['mmin'], row['mmax'])
    prob_m1_new.append(xp.asnumpy(pm1)*row['rate'])

for i, row in downsampled_old.iterrows():
    pm1 = power_law_primary_wrapper({'mass_1': xp.asarray(m1s)}, row['alpha'], row['mmin'], row['mmax'])
    prob_m1_old.append(xp.asnumpy(pm1)*row['rate'])

m1_new_5 = np.nanquantile(prob_m1_new, 0.05, axis=0)
m1_new_95 = np.nanquantile(prob_m1_new, 0.95, axis=0)

m1_old_5 = np.nanquantile(prob_m1_old, 0.05, axis=0)
m1_old_95 = np.nanquantile(prob_m1_old, 0.95, axis=0)

np.savetxt('/home/michael.zevin/projects/o4/gw230529/analysis_results/populations/EMBright/final/m1_contours_nsbh_embright.dat',
        np.array([m1s, m1_old_5, m1_old_95, np.mean(prob_m1_old, axis=0), m1_new_5, m1_new_95, np.mean(prob_m1_new, axis=0)]).T,
        delimiter='\t', header='m1\t m1_old_5\t m1_old_95\t m1_old_ppd\t m1_new_5\t m1_new_95\t m1_new_ppd')
