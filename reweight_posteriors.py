import bilby
import h5py
import pandas as pd
import numpy as np
from tqdm import tqdm
from pop_models_nsbh import *
import gwpopulation
from gwpopulation.cupy_utils import xp, to_numpy

xp.random.seed(200105)
outdir = "/home/michael.zevin/projects/o4/gw230529/analysis_results/"
result = bilby.core.result.read_in_result(
    outdir
    + "populations/EMBright/final/gw_only_S230529ay_nsbh_ns_spin_pairing_gaussian_result.json"
)

# luminosity distance Jacobian
dl_prior = bilby.gw.prior.UniformSourceFrame(minimum=1.0, maximum=3500.0, cosmology="Planck15", name="luminosity_distance", latex_label="$d_L$", unit="Mpc")
redshift_prior = dl_prior.get_corresponding_prior("redshift")

# load posteriors
filename = outdir + "parameter_estimation/CombinedPHMHighSpin/posterior_samples.h5"
data = h5py.File(filename, "r")
df = {}
df["mass_1"] = data["combined_imrphm_high_spin"]["posterior_samples"]["mass_1_source"]
df["mass_ratio"] = data["combined_imrphm_high_spin"]["posterior_samples"]["mass_ratio"]
df["mass_2"] = data["combined_imrphm_high_spin"]["posterior_samples"]["mass_2_source"]
df["a_1"] = data["combined_imrphm_high_spin"]["posterior_samples"]["a_1"]
df["a_2"] = data["combined_imrphm_high_spin"]["posterior_samples"]["a_2"]
df["spin_1z"] = data["combined_imrphm_high_spin"]["posterior_samples"]["spin_1z"]
df["chi_eff"] = data["combined_imrphm_high_spin"]["posterior_samples"]["chi_eff"]
df["redshift"] = data["combined_imrphm_high_spin"]["posterior_samples"]["redshift"]
df["prior"] = df["mass_1"]*(1 + data["combined_imrphm_high_spin"]["posterior_samples"]["redshift"])**2*\
              redshift_prior.prob(data["combined_imrphm_high_spin"]["posterior_samples"]["redshift"])

for key in df.keys():
    df[key] = xp.asarray(df[key])

model = bilby.hyper.model.Model([power_law_primary_wrapper,
                                truncnorm_q_nsbh,
                                gwpopulation.models.redshift.PowerLawRedshift(z_max=0.25),
                                ns_spin,
                                beta_bh_spin])

event_weights = []
for i, row in tqdm(result.posterior.iterrows(), total=len(result.posterior)):
    model.parameters = row
    p_pop = model.prob(df)
    numerator = p_pop / df["prior"]
    denom = xp.mean(numerator)
    event_weights.append(to_numpy(numerator / denom))
weights_flat = np.sum(np.asarray(event_weights), axis=0)
neff = int(xp.sum(weights_flat) ** 2 / np.sum(weights_flat**2))
N = 10000
print(neff)
for key in df.keys():
    df[key] = to_numpy(df[key])
df = pd.DataFrame.from_dict(df)
reweighted_post = df.sample(N, replace=True, weights=to_numpy(weights_flat))
reweighted_post.to_csv(
    outdir + "populations/EMBright/final/reweighted_posteriors_embright.dat",
    sep="\t",
    index=False,
)
