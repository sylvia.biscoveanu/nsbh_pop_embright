import bilby
import h5py
import pandas as pd
import os
import tqdm
import argparse
from bilby.core.prior import Uniform
import fitting_formulae as ff
from pop_models_nsbh import *
import gwpopulation
from gwpopulation_pipe.vt_helper import load_injection_data
from gwpopulation.cupy_utils import xp

def main(runtype, outdir, S230529ay):
    xp.random.seed(200105)

    # luminosity distance Jacobian
    dl_prior = bilby.gw.prior.UniformSourceFrame(minimum=1.0, maximum=3500.0, cosmology='Planck15', name='luminosity_distance', latex_label='$d_L$', unit='Mpc')
    redshift_prior = dl_prior.get_corresponding_prior('redshift')

    # load posteriors
    posteriors = []
    if S230529ay:
        label = 'gw_only_S230529ay_nsbh_ns_spin_pairing_'+runtype
        filenames = {'/home/pe.o3/o3b_catalog/releases/v1/IGWN-GWTC3p0-v1-GW200105_162426_PEDataRelease_mixed_cosmo.h5': 'C01:Mixed',
                     '/home/pe.o3/o3b_catalog/releases/v1/IGWN-GWTC3p0-v1-GW200115_042309_PEDataRelease_mixed_cosmo.h5': 'C01:Mixed',
                     '/home/michael.zevin/projects/o4/gw230529/analysis_results/parameter_estimation/CombinedPHMHighSpin/posterior_samples.h5': 'combined_imrphm_high_spin'}
    else:
        label = 'gw_only_nsbh_ns_spin_pairing_'+runtype
        filenames = {'/home/pe.o3/o3b_catalog/releases/v1/IGWN-GWTC3p0-v1-GW200105_162426_PEDataRelease_mixed_cosmo.h5': 'C01:Mixed',
                     '/home/pe.o3/o3b_catalog/releases/v1/IGWN-GWTC3p0-v1-GW200115_042309_PEDataRelease_mixed_cosmo.h5': 'C01:Mixed'}
    for event in filenames.keys():
        data = h5py.File(event, 'r')
        df = pd.DataFrame()
        df['mass_1'] = data[filenames[event]]['posterior_samples']['mass_1_source']
        df['mass_ratio'] = data[filenames[event]]['posterior_samples']['mass_ratio']
        df['a_1'] = data[filenames[event]]['posterior_samples']['a_1']
        df['a_2'] = data[filenames[event]]['posterior_samples']['a_2']
        df['spin_1z'] = data[filenames[event]]['posterior_samples']['spin_1z']
        df['redshift'] = data[filenames[event]]['posterior_samples']['redshift']
        df['prior'] = df['mass_1']*(1 + data[filenames[event]]['posterior_samples']['redshift'])**2*\
                      redshift_prior.prob(data[filenames[event]]['posterior_samples']['redshift'])
        posteriors.append(df)

    # hierarchical model
    if runtype == 'gaussian':
        q_model = truncnorm_q_nsbh
    elif runtype == 'power_law':
        q_model = power_law_q_nsbh
    model = bilby.hyper.model.Model([power_law_primary_wrapper,
                                     q_model,
                                     gwpopulation.models.redshift.PowerLawRedshift(z_max=0.25),
                                     ns_spin,
                                     beta_bh_spin])

    # selection effects
    injection_file = '/home/reed.essick/rates+pop/o1+o2+o3-sensitivity-estimates/LIGO-T2100377-v2/o1+o2+o3_nsbhpop_real+semianalytic-LIGO-T2100377-v2.hdf5'
    all_found_injections = load_injection_data(injection_file, ifar_threshold=4)
    vt_model = bilby.hyper.model.Model([power_law_primary_wrapper,
                                     q_model,
                                     gwpopulation.models.redshift.PowerLawRedshift(z_max=0.25),
                                     ns_spin,
                                     beta_bh_spin])
    N_EVENTS = len(posteriors)
    resampling_vt = gwpopulation.vt.ResamplingVT(model=vt_model, data=all_found_injections, n_events=N_EVENTS)

    # likelihood
    fast_likelihood = gwpopulation.hyperpe.HyperparameterLikelihood(
            posteriors=posteriors, hyper_prior=model, selection_function=resampling_vt)

    # prior
    fast_priors = bilby.core.prior.PriorDict()
    fast_priors['alpha'] = Uniform(minimum=-4, maximum=12, latex_label='$\\alpha$')
    fast_priors['mmin'] = Uniform(minimum=2, maximum=10, latex_label='$m_{\mathrm{BH}, \\min}$')
    fast_priors['mmax'] = Uniform(minimum=8, maximum=20, latex_label='$m_{\mathrm{BH}, \\max}$')
    fast_priors['m2_max'] = Uniform(minimum=1.97, maximum=2.7, latex_label='$m_{\mathrm{NS}, \\max}$')
    if runtype == 'gaussian':
        fast_priors['mu'] = Uniform(minimum=0.1, maximum=0.6, latex_label='$\\mu$')
        fast_priors['sigma'] = Uniform(minimum=0.1, maximum=1, latex_label='$\\sigma$')
    elif runtype == 'power_law':
        fast_priors['beta'] = Uniform(minimum=-10, maximum=4, latex_label='$\\beta$')
    fast_priors['lamb'] = 0
    fast_priors['alpha_chi'] = Uniform(minimum=0.01, maximum=10, latex_label='$\\alpha_{\\chi}$')
    fast_priors['beta_chi'] = Uniform(minimum=0.01, maximum=10, latex_label='$\\beta_{\\chi}$')

    test_sample = fast_priors.sample()
    fast_likelihood.parameters = test_sample
    print(fast_likelihood.log_likelihood_ratio())

    hyper_result = bilby.run_sampler(likelihood=fast_likelihood, priors=fast_priors, sampler='dynesty',
                                     sample='rwalk', nlive=1000, walks=5, nact=2, label=label, outdir=outdir)

    print('Calculating rate posterior')
    rate_post = []
    for i, row in tqdm.tqdm(hyper_result.posterior.iterrows(), total=len(hyper_result.posterior)):
        fast_likelihood.parameters = row.to_dict()
        rate = fast_likelihood.generate_rate_posterior_sample()
        rate_post.append(float(rate))
    hyper_result.posterior['rate'] = rate_post
    hyper_result.save_to_file(overwrite=True)
    hyper_result.plot_corner()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--S230529ay', action='store_true', help='include S230529ay')
    parser.add_argument('--outdir', default='./outdir', help='output directory')
    parser.add_argument('--runtype', default='gaussian', choices=['gaussian', 'power_law'], help='mass ratio model')
    args = parser.parse_args()
    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)
    print(args)
    main(**vars(args))
