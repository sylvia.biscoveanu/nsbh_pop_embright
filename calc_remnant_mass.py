import numpy as np
import fitting_formulae as ff
from calc_embright_fraction_realistic_EOS import load_eos_data
from tqdm import tqdm
import sys
import h5py


# load posteriors
posterior_file_high_spin = "/home/michael.zevin/projects/o4/gw230529/analysis_results/parameter_estimation/CombinedPHMHighSpin/posterior_samples.h5"
data_high_spin = h5py.File(posterior_file_high_spin, "r")
post_high = np.array(data_high_spin["combined_imrphm_high_spin"]["posterior_samples"])

posterior_file_low_spin = "/home/michael.zevin/projects/o4/gw230529/analysis_results/parameter_estimation/CombinedPHMLowSpin/posterior_samples.h5"
data_low_spin = h5py.File(posterior_file_low_spin, "r")
post_low = np.array(data_low_spin["combined_imrphm_low_spin"]["posterior_samples"])
eos_samples = load_eos_data(1, nicer=False)
np.random.seed(230529)

def calc_mrem(post):
    eos_sample = np.random.choice(eos_samples)
    chi_kep = float(eos_sample["chi_kep"])
    m2 = post["mass_2"]
    a2 = post["a_2"]
    a1z = post["spin_1z"]
    mmax = ff.mmax_spinning(a2, chi_kep, float(eos_sample["m_tov"]))
    black_hole = False
    # check if neutron star
    if (m2 > mmax) or (a2 > chi_kep):
        mrem = 0
        black_hole = True
        compactness = np.nan
        return black_hole, compactness, mrem
    m0 = ff.m_nonspinning(a2, chi_kep, m2)
    r0 = eos_sample["mr_interp"](m0)
    compactness = ff.compactness(m0, r0)
    mrem = ff.mrem_msun_spinning(post["mass_ratio"], m2, compactness, a1z, a2)
    return black_hole, compactness, float(mrem)


remnant_mass_high = []
compactness_high = []
black_hole_high = []
for i in tqdm(post_high):
    black_hole, compactness, mrem = calc_mrem(i)
    black_hole_high.append(black_hole)
    remnant_mass_high.append(mrem)
    compactness_high.append(compactness)

remnant_mass_low = []
compactness_low = []
black_hole_low = []
for i in tqdm(post_low):
    black_hole, compactness, mrem = calc_mrem(i)
    black_hole_low.append(black_hole)
    remnant_mass_low.append(mrem)
    compactness_low.append(compactness)

print(f'Black hole fraction high {np.sum(black_hole_high) / len(black_hole_high)}, low {np.sum(black_hole_low) / len(black_hole_low)}')
ns_high = np.invert(black_hole_high)
ns_low = np.invert(black_hole_low)
print(f'Neutron star remnant fraction high {np.sum(np.array(remnant_mass_high)[ns_high] > 0)/np.sum(ns_high)}, low {np.sum(np.array(remnant_mass_low)[ns_low] > 0)/np.sum(ns_low)}')
nmax = min(len(remnant_mass_low), len(remnant_mass_high))
remnant_mass_low = np.random.choice(remnant_mass_low, size=nmax, replace=False)
remnant_mass_high = np.random.choice(remnant_mass_high, size=nmax, replace=False)
np.savetxt(
    "/home/michael.zevin/projects/o4/gw230529/analysis_results/populations/EMBright/final/event_remnant_mass.dat",
    np.array([remnant_mass_high, remnant_mass_low]).T,
    header="m_rem,highSpin [Msun]\t m_rem,lowSpin [Msun]",
)
