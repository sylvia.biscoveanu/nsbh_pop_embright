import sys
from pop_models_nsbh import power_law_primary_wrapper
from gwpopulation.cupy_utils import xp
import fitting_formulae as ff
import numpy as np
import bilby
from tqdm import tqdm
from bilby.core.prior import Beta, Interped, TruncatedGaussian, PowerLaw
from calc_embright_fraction_realistic_EOS import load_eos_data


def main(filename):
    m1s = xp.linspace(2, 20, 101)
    result = bilby.core.result.read_in_result(filename)
    rate_post = result.posterior["rate"]
    eos_samples = load_eos_data(1, nicer=False)
    max_mass = [sample["maximum_mass_pop"] for sample in eos_samples]

    total_ejecta_mass = []
    total_embright_mrem0 = []
    total_embright_mrem01 = []
    for i, row in tqdm(result.posterior.iterrows(), total=len(result.posterior)):
        size = int(np.round(rate_post[i]))
        pm1 = power_law_primary_wrapper(
            {"mass_1": xp.asarray(m1s)}, row["alpha"], row["mmin"], row["mmax"]
        )
        m1_prior = Interped(
            xp.asnumpy(m1s), xp.asnumpy(pm1), minimum=row["mmin"], maximum=row["mmax"]
        )
        m1_samples = m1_prior.sample(size)
        chiBH_prior = Beta(row["alpha_chi"], row["beta_chi"])
        chiBH_sample = chiBH_prior.sample(size)
        cos_tiltBH_sample = np.random.uniform(-1, 1, size)
        inds = np.where(np.array(max_mass) >= row["m2_max"])[0]
        ind = np.random.choice(inds)
        eos_sample = eos_samples[ind]
        chiNS_sample = np.random.uniform(0, float(eos_sample["chi_kep"]), size)
        mrem_arr = []
        for j, m1_sample in enumerate(m1_samples):
            if "gaussian" in filename:
                pq_prior = TruncatedGaussian(
                    row["mu"],
                    row["sigma"],
                    minimum=1.0 / m1_sample,
                    maximum=np.minimum(row["m2_max"] / m1_sample, 1),
                )
            elif "power_law" in filename:
                pq_prior = PowerLaw(
                    row["beta"],
                    minimum=1.0 / m1_sample,
                    maximum=np.minimum(row["m2_max"] / m1_sample, 1),
                )
            else:
                raise ValueError(
                    "Unrecognized pairing function, must be either gaussian or power_law!"
                )
            q_sample = pq_prior.sample()
            m2_sample = q_sample * m1_sample
            m0 = ff.m_nonspinning(
                chiNS_sample[j], float(eos_sample["chi_kep"]), m2_sample
            )
            a1z = chiBH_sample[j] * cos_tiltBH_sample[j]
            try:
                r0 = eos_sample["mr_interp"](m0)
                compactness = ff.compactness(m0, r0)
                mrem_ind = ff.mrem_msun_spinning(
                    q_sample, m2_sample, compactness, a1z, chiNS_sample[j]
                )
                mrem_arr.append(float(mrem_ind))
            except ValueError:
                mrem_arr.append(0)
        total_ejecta_mass.append(np.sum(mrem_arr))
        total_embright_mrem0.append(np.sum(np.array(mrem_arr) > 0))
        total_embright_mrem01.append(np.sum(np.array(mrem_arr) > 0.1))
    np.savetxt(
        "{}/{}_total_embright.dat".format(result.outdir, result.label),
        np.array([total_ejecta_mass, total_embright_mrem0, total_embright_mrem01]).T,
        header="Total ejecta mass [Msun Gpc^-3 yr^-1]\t Total Mrem>0 [Gpc^-3 yr^-1]\t Total Mrem>0.1 Msun [Gpc^-3 yr^-1]\t",
    )


if __name__ == "__main__":
    main(sys.argv[1])
