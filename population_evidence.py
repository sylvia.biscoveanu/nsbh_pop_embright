import bilby
import h5py
import pandas as pd
import pdb
import numpy as np
from tqdm import tqdm
from pop_models_nsbh import *
import gwpopulation
from gwpopulation.cupy_utils import xp

xp.random.seed(200105)
outdir = "/home/michael.zevin/projects/o4/gw230529/analysis_results/"
result = bilby.core.result.read_in_result(
    outdir
    + "populations/EMBright/final/gw_only_nsbh_ns_spin_pairing_gaussian_result.json"
)

# load posteriors
filename = outdir + "parameter_estimation/CombinedPHMHighSpin/posterior_samples.h5"
data = h5py.File(filename, "r")
df = {}
df["mass_1"] = data["combined_imrphm_high_spin"]["posterior_samples"]["mass_1_source"]
df["mass_ratio"] = data["combined_imrphm_high_spin"]["posterior_samples"]["mass_ratio"]
df["a_1"] = data["combined_imrphm_high_spin"]["posterior_samples"]["a_1"]
df["a_2"] = data["combined_imrphm_high_spin"]["posterior_samples"]["a_2"]
df["redshift"] = data["combined_imrphm_high_spin"]["posterior_samples"]["redshift"]
df["prior"] = df["mass_1"]*(1 + data["combined_imrphm_high_spin"]["posterior_samples"]["redshift"])**2

for key in df.keys():
    df[key] = xp.asarray(df[key])

model = bilby.hyper.model.Model([power_law_primary_wrapper,
                                truncnorm_q_nsbh,
                                ns_spin,
                                beta_bh_spin])

m1_min = 2.32201715543291
m1_max = 7.248377635440223
m2_min = 1 # fixed for NSBH-pop
m2_max = m1_min
weights = []
for i, row in tqdm(result.posterior.iterrows(), total=len(result.posterior)):
    # normalize the model over the bounds above
    row['mmax'] = min(m1_max, row['mmax'])
    row['mmin'] = max(m1_min, row['mmin'])
    row['m2_max'] = min(m2_max, row['m2_max'])
    norm_a2 = 1/0.7
    model.parameters = row
    p_pop = model.prob(df)*norm_a2
    weights.append(p_pop/df['prior'])
log_weight = xp.log10(xp.mean(xp.asarray(weights)))
bf_file = open('/home/sylvia.biscoveanu/s230529ay/macros/embright_evidence.tex','w')
bf_file.write(f"\\newcommand{{\\logPopulationEvidence}}{{{log_weight:.2f}}}\n")
print(log_weight)
