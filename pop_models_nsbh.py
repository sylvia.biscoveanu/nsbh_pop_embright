import gwpopulation
from gwpopulation.cupy_utils import xp

def truncnorm_q_nsbh(dataset, mu, sigma, m2_max):
    """
    Return p(q | m1) assuming a truncated Gaussian
    Assume m2_min = 1
    -----------
    Parameters:
    dataset: dataframe of 2D arrays of binary parameters
    mu: mean of truncated Gaussian
    sigma: standard deviation of truncated Gaussian
    m2_max: maximum of the secondary mass distribution
    """
    qmin = 1./dataset['mass_1']
    qmax = xp.minimum(m2_max/dataset['mass_1'], 1)
    return gwpopulation.utils.truncnorm(dataset['mass_ratio'], mu, sigma, qmax, qmin)

def power_law_q_nsbh(dataset, beta, m2_max):
    """
    Return p(q | m1) assuming a power law
    Assume m2_min = 1
    -----------
    Parameters:
    dataset: dataframe of 2D arrays of binary parameters
    beta: power-law index
    m2_max: maximum of the secondary mass distribution
    """
    qmin = 1./dataset['mass_1']
    qmax = xp.minimum(m2_max/dataset['mass_1'], 1)
    return gwpopulation.utils.powerlaw(dataset['mass_ratio'], beta, qmax, qmin)

def beta_bh_spin(dataset, alpha_chi, beta_chi):
    """
    Return p(chi1) assuming a Beta distribution
    Bounded between 0 and 1
    -----------
    Parameters:
    dataset: dataframe of 2D arrays of binary parameters
    alpha_chi: Beta distribution alpha parameter
    beta_chi: Beta distribution beta parameter
    """
    return gwpopulation.utils.beta_dist(dataset['a_1'], alpha_chi, beta_chi)

def ns_spin(dataset):
    """
    Return p(chi2) assuming breakup spin of 0.7
    Does not change shape of distribution
    -----------
    Parameters:
    dataset: dataframe of 2D arrays of binary parameters
    """
    prob = xp.ones(dataset['a_2'].shape)
    breakup_spin = (dataset['a_2']>0.7)
    prob[breakup_spin] = 0
    return prob

def power_law_primary_wrapper(dataset, alpha, mmin, mmax):
    """
    Return p(m1) assuming truncated power law
    Fraction in Gaussian component = 0
    -----------
    Parameters:
    dataset: dataframe of 2D arrays of binary parameters
    alpha: power-law index
    mmin: minimum mass
    mmax: maximum mass
    """
    if mmin > mmax:
        return xp.zeros(dataset['mass_ratio'].shape)
    else:
        return gwpopulation.models.mass.two_component_single(
                dataset['mass_1'], alpha, mmin, mmax, 0, 50, 5)

